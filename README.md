# chart_install_gitlab_runner

This repository is demo for use helm chart to establish gitlab runner on k8s

## steps

Add Gitlab Helm repository:

```shell
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
```

## pull target helm

```shell
helm pull gitlab/gitlab-runner --version 0.61.0
tar xf gitlab-runner-0.61.0.tgz 
cd gitlab-runner/
```

## update helm_values.yaml

```yaml
concurrent: 5
logFormat: json
rbac:
  create: true
  rules:
    - apiGroups: [""]
      resources: ["configmaps", "events", "pods", "pods/attach", "pods/exec", "secrets", "services"]
      verbs: ["get", "list", "watch", "create", "patch", "update", "delete"]
runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        namespace = "{{.Release.Namespace}}"
        image = "alpine"
```

## create gitlab runner from gitlab runner setup

copy the token 

```shell
helm install --namespace gitlab-runner --create-namespace --atomic --debug --timeout 120s --set gitlabUrl=$runner_url  --set runnerToken=$runner_token --values helm_values.yaml  gitlab-runner gitlab/gitlab-runner --version 0.61.0
```

## create gitlab-runner with k8s flow

![alt text](image.png)